require 'socket'
require 'watir-webdriver'
require 'rspec'
require 'net/smtp'
require 'rubygems'
require 'xmlsimple'
=begin
THESE WILL HELP CLEAN THE GARBAGE DATA
/*SELECT * FROM user order by CREATE_DATE desc;

select classroom_id from  bigideas_db.classroom where creator_id = '93268938-1bb5-4096-91e5-4105a5084feb';

delete from organization_user where user_id = '93268938-1bb5-4096-91e5-4105a5084fe';
delete from user_access_log where user_id = '93268938-1bb5-4096-91e5-4105a5084feb';
delete from classroom_user where user_id = '93268938-1bb5-4096-91e5-4105a5084fe';
delete from classroom_course where classroom_id = '4bee14df-ab75-4c96-b427-c8fe22d4aa31' ;
delete from classroom where creator_id = '93268938-1bb5-4096-91e5-4105a5084fe';
delete from user where user_id = '93268938-1bb5-4096-91e5-4105a5084fe';
#additional tables include last_visited, student_assignment_question, student_assignment, and assignment 
*/

=end

=begin
THIS WILL CREATE A TEACHER WITH AS MANY CLASSES AS ARE IN THE courses ARRAY. 
FOR EACH CLASS 3 STUDENTS WILL BE CREATED AND A NUMBER OF ASSIGNMENTS FROM THE XML IN THIS DIRECTORY WILL ALSO BE GENERATED
#####NECESSARY INPUT#####
ACCESS CODE FOR DEMO
DISTRICT NAME AS SEEN IN LIST
DISTRICT SHORT NAME 
See Main() 
=end

class Course

    attr_reader :course_name
	attr_reader :startDate
	attr_reader :endDate
	attr_reader :classroomName
	attr_reader :accessCode
	
	attr_writer :course_name
	attr_writer :startDate
	attr_writer :endDate
	attr_writer :classroomName
	attr_writer :accessCode
	
	
	
	def initialize(course_name,startDate,endDate,classroomName, accessCode)
		@course_name = course_name
        @startDate = startDate
        @endDate = endDate
        @classroomName = classroomName
		@accessCode  = accessCode
	end
	
	def display_details()
	  puts "course_name #@course_name"
	  puts "startDate #@startDate"
	  puts "endDate #@endDate"
	  puts "classroomName #@classroomName"
	  puts "accessCode  #@accessCode "
	end
end

def RemoveStartAndEndQuotes(str)
  if str[0] == ('"')
    str = str.slice(1..-1)
  end
  if str[str.length - 1] == ('"')
    str = str.slice(0..-2)
  end
  return str
end 

def LoginToBIM(browser, username, password)
  browser.goto "#{SitePrefix}.bigideasmath.com/BIM/login"
  browser.text_field(:name => 'username').wait_until_present
  browser.text_field(:name => 'username').set username
  browser.text_field(:name => 'password').set password
  browser.button(:name => 'submit').click
end

def CreateStudent(browser, classAccessCode, firstName, lastName, password)
  browser.goto "#{SitePrefix}.bigideasmath.com/BIM/register"
  browser.refresh
  browser.text_field(:id => 'enter-code-input').wait_until_present
  browser.text_field(:id => 'enter-code-input').set classAccessCode 
  browser.send_keys :enter
  browser.text_field(:id => 'student-first-name').wait_until_present
  browser.text_field(:id => 'student-first-name').set 'DEMO'
  browser.text_field(:id => 'student-last-name').set lastName 
  browser.text_field(:id => 'student-pass').set password
  browser.text_field(:id => 'student-pass-confirm').set password
  browser.send_keys :enter
  browser.input(:id => "student-username").wait_until_present
  studentUserName = browser.input(:id => "student-username").value 
  return studentUserName
end

def CreateClass(browser, courseName, className, startDate,endDate)
  browser.goto "#{SitePrefix}.bigideasmath.com/BIM/teacher/classmanagement"
  browser.refresh
  sleep 5
  browser.element(:xpath => "//*[@id='new-class']/li/h2/img").wait_until_present
  browser.element(:xpath => "//*[@id='new-class']/li/h2/img").click
  browser.text_fields(:name => "new-class-name").last.set className
  browser.text_fields(:name=> "new-start-date").last.set startDate
  browser.text_fields(:name=> "new-end-date").last.set endDate
  browser.options(:text=> courseName).last.click
  browser.buttons(:class => "save btn orange").last.click 
  sleep 5
  browser.images(:src => '/BIM/resources/images/register/view-info.png').last.click
  browser.inputs(:class => "access-code disabled").last.wait_until_present
  classAccessCode = browser.inputs(:class => "access-code disabled").last.value
  return classAccessCode
end

def CreateAnAssignment (browser, classroomName, assessmentId, courseContentId, assessmentType, startDate,endDate,name,liveTutor,calculator)
  browser.div(:id => 'idGlobalClassName').click
  classroomId = browser.li(:text => classroomName).id
  urlOfAssignment = "#{SitePrefix}.bigideasmath.com/BIM//teacher/assignment?assessmentId=#{assessmentId}&assessmentType=#{assessmentType}&classroomId=#{classroomId}&courseContentId=#{courseContentId}"
  browser.goto urlOfAssignment
  browser.span(:class => 'bi-icon bi-icon-plus-blue').click
  browser.link(:href => '#idAddNewAssignmentCollapseStep1').click
  if assessmentType == 'HOMEWORK'
    if name[-1].to_i % 2 == 0
      browser.label(:text => 'Even').click
	else 
	  browser.label(:text => 'Average').click
	end
    if liveTutor == true
      browser.div(:class => 'bi-checkbox bi-checkbox-live-tutor').span(:class => 'bi-checkbox-image').click
    end
  else
    if assessmentType == 'QUIZ'
	  browser.label(:text => 'Same').click
	else
      browser.label(:text => 'Random').click
	end
  end
  if calculator == true
    browser.div(:class => 'bi-checkbox bi-checkbox-calculator').span(:class => 'bi-checkbox-image').click
  end
  sleep 1
  browser.div(:id => 'idAddNewAssignmentCollapseStep1').div(:class => 'panel-footer').button(:class => 'btn btn-warning btn-lg bi-next-step-button').wait_until_present
  browser.div(:id => 'idAddNewAssignmentCollapseStep1').div(:class => 'panel-footer').button(:class => 'btn btn-warning btn-lg bi-next-step-button').click
  if browser.label(:text => 'All students').present? == false  #added double check due to odd behaviour with practice tests?
    browser.link(:href => '#idAddNewAssignmentCollapseStep2').click
  end
  browser.label(:text => 'All students').wait_until_present
  browser.label(:text => 'All students').click
  sleep 1
  browser.div(:id => 'idAddNewAssignmentCollapseStep2').div(:class => 'panel-footer').button(:class => 'btn btn-warning btn-lg bi-next-step-button').wait_until_present
  browser.div(:id => 'idAddNewAssignmentCollapseStep2').div(:class => 'panel-footer').button(:class => 'btn btn-warning btn-lg bi-next-step-button').click  
  if startDate != ''
    sleep 1
	browser.div(:id => 'idAddNewAssignmentCollapseStep3').text_field(:class => 'bi-button bi-datepicker-input hasDatepicker').wait_until_present
    browser.div(:id => 'idAddNewAssignmentCollapseStep3').text_field(:class => 'bi-button bi-datepicker-input hasDatepicker').set startDate
    browser.send_keys :enter
  end
  sleep 1
  browser.div(:id => 'idAddNewAssignmentCollapseStep3').div(:class => 'panel-footer').button(:class => 'btn btn-warning btn-lg bi-next-step-button').wait_until_present
  browser.div(:id => 'idAddNewAssignmentCollapseStep3').div(:class => 'panel-footer').button(:class => 'btn btn-warning btn-lg bi-next-step-button').click
  if endDate != ''
    sleep 1
    browser.div(:id => 'idAddNewAssignmentCollapseStep4').text_field(:class => 'bi-button bi-datepicker-input hasDatepicker').wait_until_present
    browser.div(:id => 'idAddNewAssignmentCollapseStep4').text_field(:class => 'bi-button bi-datepicker-input hasDatepicker').set endDate
    browser.send_keys :enter
  end
  sleep 1
  browser.div(:id => 'idAddNewAssignmentCollapseStep4').div(:class => 'panel-footer').button(:class => 'btn btn-warning btn-lg bi-next-step-button').wait_until_present
  browser.div(:id => 'idAddNewAssignmentCollapseStep4').div(:class => 'panel-footer').button(:class => 'btn btn-warning btn-lg bi-next-step-button').click
  sleep 1
  browser.div(:id => 'idAddNewAssignmentCollapseStep5').text_field(:class => 'form-control bi-assignment-step-version-name').wait_until_present
  browser.div(:id => 'idAddNewAssignmentCollapseStep5').text_field(:class => 'form-control bi-assignment-step-version-name').set name
  sleep 1
  browser.button(:class => 'btn btn-warning bi-queue-add-button').click
  sleep 1
  browser.execute_script("window.confirm = function() {return true}")
  browser.button(:id => 'idAssignButton').click
  sleep 5
end

def CreateTeacherAccount(browser, accessCode, firstName, lastName, email, password, districtName)
  browser.goto "#{SitePrefix}.bigideasmath.com/BIM/register"
  browser.text_field(:id => 'enter-code-input').set accessCode 
  browser.send_keys :enter
  browser.text_field(:id => 'teacher-first-name').wait_until_present
  browser.text_field(:id => 'teacher-first-name').set firstName
  browser.text_field(:id => 'teacher-last-name').set lastName
  browser.text_field(:id => 'teacher-email').set email
  browser.text_field(:id => 'teacher-email-confirm').set email
  browser.text_field(:id => 'teacher-pass').set password
  browser.text_field(:id => 'teacher-pass-confirm').set password
  browser.send_keys :enter
  browser.options[0].wait_until_present
  browser.options[0].click
  browser.button(:class => "btn btn-link btn-add").click
  browser.element(:xpath => "//*[@id='teacher-form-two']/div[3]/div/button").click
  browser.element(:text => "Congrats you are all registered. Click next button to log in.").wait_until_present
  browser.element(:xpath => "//*[@id='teacher-form-two']/div[3]/div/button").click
end

def sendEmail(districtName, accessCode, password, teachEmail, courses, students, studentsPerClass, startDate, endDate)	
loopLength = (studentsPerClass * courses.length) - 1
messageBody = "<!DOCTYPE HTML><html><head><title>Account Creation</title></head><body>"
messageBody += "<h2>The following Accounts were created for #{districtName} on <a href=\"http://#{SitePrefix}.bigideasmath.com\">#{SitePrefix}.bigideasmath.com</a></h2>"
messageBody += "<h3>Dates: #{startDate} through #{endDate}</h3>"
messageBody += "<h3>Teacher: #{teachEmail}</h3>"
messageBody += "<h3>All Passwords are #{password}</h3>"
for i in 0..loopLength
   if i % studentsPerClass == 0
      messageBody += "<p><b>#{courses[i/studentsPerClass].course_name} : #{courses[i/studentsPerClass].accessCode}</b></p>"
   end
   messageBody += "<p>#{students[i]}</p>"	  
end
messageBody += "</body></html>"

toField = 'Jason Klins <jklins@larsontexts.com>;'
emailArray = ['jklins@larsontexts.com']

msgstr = <<END_OF_MESSAGE
From: BIM Account Creation <tech_support@larsontexts.com>
To: #{toField}
MIME-Version: 1.0
Content-type: text/html
Subject: BIM #{districtName} Account Creation on #{SitePrefix}
Message-Id: <unique.message.id.string@example.com>
#{messageBody}
END_OF_MESSAGE

	Net::SMTP.start('smtp.emailsrvr.com', 25, 'larsontexts.com', 'tech_support@larsontexts.com','ttttssss', :plain) do |smtp|
	  smtp.send_message msgstr,
                'tech_support@larsontexts.com',
                emailArray
	end
	p 'Sent email'
end

############################START MAIN#######################################

SitePrefix = 'DEMO'
#prefix options: DEMO - or - UAT

def main ()
  #####INPUT#####   *Make sure to also set SitePrefix between UAT and DEMO
  districtShortName = 	'Oregon' #used in teacher email and student id, after white space is removed 
  accessCode 		= 	'M5NE-2HGG-5AZP'   #demo = 'D7PG-NDJ7-4AZ4' #uat and prod = 'GPKE-AP7G-5GJB'
  districtName 		= 	 districtShortName #'TESTING DISTRICT #1' #OBSOLETED demo = 'BIG IDEAS DEMO DISTRICT' #uat and prod = 'TESTING DISTRICT #1'
  password 			= 	'OregonReviewDemo1'
  startDate 		= 	'02/02/2016'
  endDate 			= 	'06/02/2016'
  withAssignments   =   true
  
  firstName 		= 	'DEMO' # This is the first name used for all accounts *NOTE: This puts a 'd' at the start of all student usernames
  lastName 			= 	districtShortName.gsub(/\s+/, "") #Removes all white space from short name
  email 			= 	"Teacher@#{lastName}.com" #This can be changed to whatever, it just needs to be unique
  studentsPerClass 	= 	3 #this number of students will be created for each class - so if it's 3 and there's 3 classes, 9 students are created
  
 #(course_name,startDate,endDate,classroomName, accessCode) course_name is the Display_Name in the database
  courses = []
  #courses.push(Course.new('Advanced 1: A Bridge to Success', startDate, endDate, 'MSBR_Adv1', ''))
  #courses.push(Course.new('Advanced 1: California', startDate, endDate, 'MSCA_Adv1', ''))
  courses.push(Course.new('Advanced 1: Common Core', startDate, endDate, 'MSCC_Adv1', ''))
  #courses.push(Course.new('Advanced 1: Florida', startDate, endDate, 'MSFL_Adv1', ''))
  #courses.push(Course.new('Advanced 2: A Bridge to Success', startDate, endDate, 'MSBR_Adv2', ''))
  #courses.push(Course.new('Advanced 2: California', startDate, endDate, 'MSCA_Adv2', ''))
  courses.push(Course.new('Advanced 2: Common Core', startDate, endDate, 'MSCC_Adv2', ''))
  #courses.push(Course.new('Advanced 2: Florida', startDate, endDate, 'MSFL_Adv2', ''))
  #courses.push(Course.new('Algebra 1: A Bridge to Success', startDate, endDate, 'HSBR_Alg1', ''))
  #courses.push(Course.new('Algebra 1: California', startDate, endDate, 'MSCA_Alg1', ''))
  #courses.push(Course.new('Algebra 1: Florida', startDate, endDate, 'MSFL_Alg1', ''))
  #courses.push(Course.new('Algebra 1: Texas', startDate, endDate, 'HSTX_Alg1', ''))
  #courses.push(Course.new('Algebra 2: A Bridge to Success', startDate, endDate, 'HSBR_Alg2', ''))
  courses.push(Course.new('Algebra 2: Common Core', startDate, endDate, 'HSCC_Alg2', ''))
  #courses.push(Course.new('Algebra 2: Texas', startDate, endDate, 'HSTX_Alg2', ''))
  courses.push(Course.new('Blue: Common Core', startDate, endDate, 'MSCC_Blue', ''))
  #courses.push(Course.new('Course 1: A Bridge to Success', startDate, endDate, 'MSBR_Course1', ''))
  #courses.push(Course.new('Course 1: California', startDate, endDate, 'MSCA_Course1', ''))
  #courses.push(Course.new('Course 1: Florida', startDate, endDate, 'MSFL_Course1', ''))
  #courses.push(Course.new('Course 2 Accelerated: A Bridge to Success', startDate, endDate, 'MSBR_Course2_Accel', ''))
  #courses.push(Course.new('Course 2 Accelerated: California', startDate, endDate, 'MSCA_Course2_Accel', ''))
  #courses.push(Course.new('Course 2: A Bridge to Success', startDate, endDate, 'MSBR_Course2', ''))
  #courses.push(Course.new('Course 2: California', startDate, endDate, 'MSCA_Course2', ''))
  #courses.push(Course.new('Course 2: Florida', startDate, endDate, 'MSFL_Course2', ''))
  #courses.push(Course.new('Course 3: A Bridge to Success', startDate, endDate, 'MSBR_Course3', ''))
  #courses.push(Course.new('Course 3: California', startDate, endDate, 'MSCA_Course3', ''))
  #courses.push(Course.new('Geometry: A Bridge to Success', startDate, endDate, 'HSBR_Geom', ''))
  courses.push(Course.new('Geometry: Common Core', startDate, endDate, 'HSCC_Geom', ''))
  #courses.push(Course.new('Geometry: Texas', startDate, endDate, 'HSTX_Geom', ''))
  courses.push(Course.new('Green: Common Core', startDate, endDate, 'MSCC_Green', ''))
  courses.push(Course.new('HS Algebra 1: Common Core', startDate, endDate, 'HSCC_Alg1', ''))
  courses.push(Course.new('Integrated I', startDate, endDate, 'HS_Int_Math_I', ''))
  courses.push(Course.new('Integrated II', startDate, endDate, 'HS_Int_Math_II', ''))
  courses.push(Course.new('Integrated III', startDate, endDate, 'HS_Int_Math_III', ''))
  courses.push(Course.new('MS Algebra 1: Common Core', startDate, endDate, 'MSCC_Purple', ''))
  #courses.push(Course.new('PreAlgebra Florida', startDate, endDate, 'MSFL_Prealgebra', ''))
  courses.push(Course.new('Red Accelerated: Common Core', startDate, endDate, 'MSCC_Accel', ''))
  courses.push(Course.new('Red: Common Core', startDate, endDate, 'MSCC_Red', ''))

  
  
  
  liveTutor = true
  calculator = true
  students = []
    
  browser = Watir::Browser.new
  browser.driver.manage.timeouts.implicit_wait = 60
  
  
  CreateTeacherAccount(browser, accessCode, firstName, lastName, email, password, districtName)
  p "created teacher: #{email} pw: #{password}"
  
  ######Login as Teacher#####
  LoginToBIM(browser, email,password)
  
  ######CreateClass - returns accessCode#####
  courses.each do |crs|
    crs.accessCode = CreateClass(browser, crs.course_name, crs.classroomName, crs.startDate, crs.endDate)
	p "#{crs.course_name} : #{crs.accessCode}"
  end
 
  ######Create Student - returns username#####
  courses.each do |course|
  #course = courses[0]
    for i in 0..(studentsPerClass - 1)
      students.push (CreateStudent(browser, course.accessCode, firstName, lastName, password))
	  p "Created Student: #{students[-1]} in #{course.classroomName}"
    end
  end

  if withAssignments == true 
    ######Create Assignments for each class#####
    LoginToBIM(browser, email, password)
    #xml ending in "_4assignments" has Chapter test, Mid Quiz, 1.1 and 1.4 exercises
    myXml = XmlSimple.xml_in(".//XML\\AllCoursesChapterOneAssignmentsOn#{SitePrefix}_4assignments.xml", { 'KeyAttr' => 'display_name' } )
    courses.each do |course|
      myXml['Book'][course.course_name]['AssessSortOrder'].each do |assess|
        assignName = "#{assess['AssessType'][0]} #{RemoveStartAndEndQuotes(assess['AssessName'][0])}"
	    p "#{assignName} #{course.classroomName}"
	    CreateAnAssignment(browser, course.classroomName, assess['AssessmentId'][0], assess['CourseContentId'][0], assess['AssessType'][0], startDate, endDate, assignName, liveTutor, calculator)
      end
    end
  end

  sendEmail(districtName, accessCode, password, email, courses, students, studentsPerClass, startDate, endDate) 
  browser.close unless browser.nil?  
end

##############################END MAIN#######################################


main()


