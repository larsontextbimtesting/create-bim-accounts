require 'socket'
require 'watir-webdriver'
require 'rspec'
require 'net/smtp'
require 'rubygems'
require 'xmlsimple'

myXml = XmlSimple.xml_in("AllCoursesChapterOneAssignmentsOnUAT.xml", { 'KeyAttr' => 'display_name' } )

p myXml['Book']['Integrated I']['AssessSortOrder'][0]['AssessDesc'][0]
p myXml['Book']['Integrated II']['AssessSortOrder'][0]['AssessDesc'][0]
p myXml['Book']['Integrated I']['AssessSortOrder'].length

def RemoveStartAndEndQuotes(str)
  if str[0] == ('"')
    str = str.slice(1..-1)
  end
  if str[str.length - 1] == ('"')
    str = str.slice(0..-2)
  end
  return str
end 


myXml['Book']['Integrated II']['AssessSortOrder'].each do |assess|
  assignName = "#{assess['AssessType'][0]} #{RemoveStartAndEndQuotes(assess['AssessName'][0])}"
  p "assessment_id: #{assess['AssessmentId'][0]} course_content_id: #{assess['CourseContentId'][0]} type: #{assess['AssessType'][0]} name: #{assignName}"
end

