#load "./HSCC_Alg2_test_mapsSmaller.csv"

=begin
THIS IS THE QUERY THAT CREATED THE INPUT CSV
SELECT c.Name, c.Display_name, c.Short_name, a.TYPE, cc.Name, cc.DESCRIPTION, cc.sort_order, a.COURSE_CONTENT_ID, a.ASSESSMENT_ID
FROM assessment a
left join course_content cc on cc.course_content_id = a.course_content_id
left join course c on c.course_id = cc.course_id 
where (cc.name like '1.%' or (cc.name like 'Chapter%' and Number = 1)) and a.active = 1
order by SHORT_NAME, cc.SORT_ORDER;
=end

$headerOne = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
$headerTwo = "<data-set xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
$footer = "</data-set>"
$currentChap = ""
$currentBook = ""
$shortName = ""
$counter = 0
filein = ".//CSVs\\AllCoursesChapterOneAssignmentsOnUAT_4assignments.csv" #input file name (must be in this files dir. must have headers on first line and data starting on second)
fileoutName = ".//XML\\AllCoursesChapterOneAssignmentsOnUAT_4assignments.xml"  #output file name
fileout = File.open(fileoutName, "w")

File.open(filein, "r").each_line do |row|
  currentRow = row.split(",")
  if $counter == 1
		$currentBook = currentRow[0]
		$displayName = currentRow[1]
		$shortName = currentRow[2]
		fileout.puts($headerOne)
		fileout.puts($headerTwo)
		fileout.puts("\t<Book bookTitle=#{$currentBook} shortName = \"#{$shortName}\" display_name = #{$displayName}>")
  end
  if $counter != 0 
	if currentRow[0] != $currentBook
			fileout.puts("\t</Book>")
		$currentBook = currentRow[0]
		$displayName = currentRow[1]
		$shortName = currentRow[2]
	fileout.puts("\t<Book bookTitle=#{$currentBook} shortName = \"#{$shortName}\" display_name = #{$displayName}>")
	end
	currentRow[8].delete!("\r\n")
	fileout.puts("\t\t<AssessSortOrder sortOrder =\"#{currentRow[6]}\">")
    fileout.puts("\t\t\t<AssessType>#{currentRow[3]}</AssessType>")
    fileout.puts("\t\t\t<AssessName>#{currentRow[4]}</AssessName>")
    fileout.puts("\t\t\t<AssessDesc>#{currentRow[5]}</AssessDesc>")
    fileout.puts("\t\t\t<AssessSortOrder>#{currentRow[6]}</AssessSortOrder>")
    fileout.puts("\t\t\t<CourseContentId>#{currentRow[7]}</CourseContentId>")
    fileout.puts("\t\t\t<AssessmentId>#{currentRow[8]}</AssessmentId>")
	fileout.puts("\t\t</AssessSortOrder>")
  end
  $counter += 1
end
fileout.puts("\t</Book>")
fileout.puts($footer)